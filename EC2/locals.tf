locals {
  asg_wait_for_elb_capacity = var.asg_wait_for_elb_capacity == "" ? var.asg_min_capacity : var.asg_wait_for_elb_capacity
}